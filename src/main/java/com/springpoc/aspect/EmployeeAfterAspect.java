package com.springpoc.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;

/**
 *
 * @author vishalsharma
 */
@Aspect
public class EmployeeAfterAspect {
 
    @After("args(name)")
    public void logStringArguments(String name){
        System.out.println("******Ithe After advice laggi aa!!****** Running After Advice. String argument passed="+name);
    }
     
    @AfterThrowing("within(com.springpoc.model.Employee)")
    public void logExceptions(JoinPoint joinPoint){
        System.out.println("******Ithe Exception suttan to baad advice laggi aa!!****** Exception thrown in Employee Method="+joinPoint.toString());
    }
     
    @AfterReturning(pointcut="execution(* getName())", returning="returnString")
    public void getNameReturningAdvice(String returnString){
        System.out.println("******Ithe After Returning advice laggi aa!!****** getNameReturningAdvice executed. Returned String="+returnString);
    }
     
}