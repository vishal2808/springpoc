package com.springpoc.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

/**
 *
 * @author vishalsharma
 */
@Aspect
public class EmployeeAnnotationAspect {
 
    @Before("@annotation(com.springpoc.aspect.Loggable)")
    public void myAdvice(){
        System.out.println("******Ithe Before advice laggi aa!!****** EmployeeAnnotationAspect ****** Executing myAdvice!!");
    }
}