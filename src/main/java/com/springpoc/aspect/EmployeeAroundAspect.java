package com.springpoc.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

/**
 *
 * @author vishalsharma
 */
@Aspect
public class EmployeeAroundAspect {
 
    @Around("execution(* com.springpoc.model.Employee.getName())")
    public Object employeeAroundAdvice(ProceedingJoinPoint proceedingJoinPoint){
        System.out.println("******Ithe Before advice laggi aa!!****** EmployeeAroundAspect ****** Before invoking getName() method");
        Object value = null;
        try {
            value = proceedingJoinPoint.proceed();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        System.out.println("******Ithe Before advice laggi aa!!****** EmployeeAroundAspect ****** After invoking getName() method. Return value="+value);
        return value;
    }
}