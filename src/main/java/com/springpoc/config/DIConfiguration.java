package com.springpoc.config;

import com.springpoc.service.EmailServiceImpl;
import com.springpoc.service.MessageService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author vishalsharma
 */
@Configuration
@ComponentScan(value={"com.springpoc.consumer"})
public class DIConfiguration {
 
    @Bean
    public MessageService getMessageService(){
        return new EmailServiceImpl();
    }
}