package com.springpoc.consumer;

import com.springpoc.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MyDIConsumerApplication implements Consumer {

    private MessageService service;

    @Autowired
    public void setService(MessageService service) {
        this.service = service;
    }

    @Override
    public void processMessage(String msg, String rec) {
        //do some msg validation, manipulation logic etc
        service.sendMessage(msg, rec);
    }
}
