package com.springpoc.consumer;

import com.springpoc.service.MessageService;

public class MyXMLConsumerApplication implements Consumer {

    private MessageService service;

    public void setService(MessageService service) {
        this.service = service;
    }

    @Override
    public void processMessage(String msg, String rec) {
        //do some msg validation, manipulation logic etc
        service.sendMessage(msg, rec);
    }
}
