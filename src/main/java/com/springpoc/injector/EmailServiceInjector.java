package com.springpoc.injector;

import com.springpoc.consumer.MyDIConsumerApplication;
import com.springpoc.consumer.Consumer;
import com.springpoc.service.EmailServiceImpl;

public class EmailServiceInjector implements MessageServiceInjector {

    @Override
    public Consumer getConsumer() {
        MyDIConsumerApplication myDIApplication = new MyDIConsumerApplication();
        myDIApplication.setService(new EmailServiceImpl());
        return myDIApplication;
    }

}
