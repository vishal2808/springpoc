package com.springpoc.injector;

import com.springpoc.consumer.Consumer;

public interface MessageServiceInjector {

    public Consumer getConsumer();
}
