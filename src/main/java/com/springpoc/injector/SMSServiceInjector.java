package com.springpoc.injector;

import com.springpoc.consumer.MyDIConsumerApplication;
import com.springpoc.consumer.Consumer;
import com.springpoc.service.SMSServiceImpl;

public class SMSServiceInjector implements MessageServiceInjector {

    @Override
    public Consumer getConsumer() {
        MyDIConsumerApplication myDIApplication = new MyDIConsumerApplication();
        myDIApplication.setService(new SMSServiceImpl());
        return myDIApplication;
    }

}
