package com.springpoc.service;

import com.springpoc.model.Employee;

/**
 *
 * @author vishalsharma
 */
public class EmployeeService {
 
    private Employee employee;
     
    public Employee getEmployee(){
        return this.employee;
    }
     
    public void setEmployee(Employee e){
        this.employee=e;
    }
}