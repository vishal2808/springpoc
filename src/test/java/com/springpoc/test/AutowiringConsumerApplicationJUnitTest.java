package com.springpoc.test;

import com.springpoc.consumer.MyDIConsumerApplication;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.springpoc.service.MessageService;
import org.junit.Assert;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(value="com.springpoc.consumer")
public class AutowiringConsumerApplicationJUnitTest {
     
    private AnnotationConfigApplicationContext context = null;
 
    @Bean
    public MessageService getMessageService() {
        return new MessageService(){
            
            @Override
            public void sendMessage(String msg, String rec) {
                System.out.println("Mock Service");
            }
        };
    }
 
    @Before
    public void setUp() throws Exception {
        context = new AnnotationConfigApplicationContext(AutowiringConsumerApplicationJUnitTest.class);
    }
     
    @After
    public void tearDown() throws Exception {
        context.close();
    }
 
    @Test
    public void test() {
        MyDIConsumerApplication app = context.getBean(MyDIConsumerApplication.class);
        app.processMessage("Hi Pankaj", "pankaj@abc.com");
        Assert.assertTrue(true);
    }
 
}
