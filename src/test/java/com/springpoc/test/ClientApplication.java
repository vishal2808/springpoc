package com.springpoc.test;

import com.springpoc.config.DIConfiguration;
import com.springpoc.consumer.Consumer;
import com.springpoc.consumer.MyDIConsumerApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author vishalsharma
 */
public class ClientApplication {
 
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DIConfiguration.class);
        Consumer app = context.getBean(MyDIConsumerApplication.class);
         
        app.processMessage("Hi Pankaj", "pankaj@abc.com");
         
        //close the context
        context.close();
    }
 
}