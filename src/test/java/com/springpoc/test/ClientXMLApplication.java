package com.springpoc.test;

import com.springpoc.consumer.Consumer;
import com.springpoc.consumer.MyXMLConsumerApplication;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author vishalsharma
 */
public class ClientXMLApplication {
 
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Consumer app = context.getBean(MyXMLConsumerApplication.class);
 
        app.processMessage("Hi Pankaj", "pankaj@abc.com");
 
        // close the context
        context.close();
    }
 
}