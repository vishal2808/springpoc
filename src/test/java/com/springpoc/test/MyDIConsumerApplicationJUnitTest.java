package com.springpoc.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.springpoc.consumer.MyDIConsumerApplication;
import com.springpoc.consumer.Consumer;
import com.springpoc.injector.MessageServiceInjector;
import com.springpoc.service.MessageService;

public class MyDIConsumerApplicationJUnitTest {

    private MessageServiceInjector injector;

    @Before
    public void setUp() {
        // mock the injector with anonymous class
        injector = new MessageServiceInjector() {

            @Override
            public Consumer getConsumer() {
                // mock the message service
                MyDIConsumerApplication myDIApplication = new MyDIConsumerApplication();
                myDIApplication.setService(new MessageService() {

                    @Override
                    public void sendMessage(String msg, String rec) {
                        System.out.println("O Paaai Saaab " + rec + ", Main kiha " + msg + "!!");

                    }
                });
                return myDIApplication;
            }
        };
    }

    @Test
    public void test() {
        Consumer consumer = injector.getConsumer();
        consumer.processMessage("Hi Pankaj", "pankaj@abc.com");
    }

    @After
    public void tear() {
        injector = null;
    }

}
