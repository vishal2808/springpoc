package com.springpoc.test;

import com.springpoc.service.EmployeeService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author vishalsharma
 */
public class SpringAOPTest {
 
    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("springAOP.xml");
        EmployeeService employeeService = ctx.getBean("employeeService", EmployeeService.class);
         
        System.out.println(employeeService.getEmployee().getName());
         
        employeeService.getEmployee().setName("Pankaj");
         
        employeeService.getEmployee().throwException();
         
        ctx.close();
    }
 
}