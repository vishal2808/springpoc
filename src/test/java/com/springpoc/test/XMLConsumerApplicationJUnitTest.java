package com.springpoc.test;

import com.springpoc.consumer.Consumer;
import com.springpoc.consumer.MyXMLConsumerApplication;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.junit.Assert;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class XMLConsumerApplicationJUnitTest {
     
    private ClassPathXmlApplicationContext context = null;
 
    @Before
    public void setUp() throws Exception {
        context = new ClassPathXmlApplicationContext("applicationContext.xml");
    }
     
    @After
    public void tearDown() throws Exception {
        context.close();
    }
 
    @Test
    public void test() {
        Consumer app = context.getBean(MyXMLConsumerApplication.class);
        app.processMessage("Hi Pankaj", "pankaj@abc.com");
        Assert.assertTrue(true);
    }
 
}
